<?php 

class SaveAndExit extends PluginBase {
    protected $storage = 'DbStorage';
    static protected $description = 'Save And Exit functionality that seems to be missing';
    static protected $name = 'SaveAndExit';

    public function init() {
        $this->subscribe('afterResponseSave');
        $this->subscribe('afterSurveyDynamicSave','afterResponseSave');
    }

    public function afterResponseSave(){

        //check if we have a form element that is requesting a logout.
        $exit = isset($_POST['exit-after-save']) ? boolval($_POST['exit-after-save']) : false;

        if($exit){
            $this->killSessionAndExit();
        }        
    }

    protected function killSessionAndExit(){
            //kill session and redirect
            //remove PHPSESSID from browser
            if ( isset( $_COOKIE[session_name()] ) )
            setcookie( session_name(), "", time()-3600, "/" );
            //clear session from globals
            $_SESSION = array();
            //clear session from disk
            session_destroy();
    }
}